// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require_tree .


$(document).on('turbolinks:load', function() {
  if ($(".alert, .notice").length) {
    $(".alert, .notice").delay(3000).slideUp('slow');
  }

  $(".alert, .notice").click(function() {
    $(this).hide();
  });

  $("#sync-tut-button").hover(function() {
    $(".hover-div").show();
  }, function() {
    $(".hover-div").hide();
  });

  $("#sync-tut-button").click(function() {
    $(".modal-bg").show();
  });

  $(".modal-bg").click(function(e) {
    if (e.target !== this) return;
    $(this).hide();
  });


  var max = 140;
  $("#bio-input").keyup(function() {
    var len = $(this).val().length;
    if (len >= max) {
      $('#char-num').text(' you have reached the character limit');
    } else {
      var char = max - len;
      $('#char-num').text(char + ' characters left');
    }
  });


});
