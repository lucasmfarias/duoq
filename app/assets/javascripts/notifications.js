function notificationPoll() {
  setTimeout(notificationRequest, 2000);
}

function notificationRequest() {
  $.get($('#topbar').data('url'));
}

$(document).on('ready', function() {
  if ($('#topbar').length > 0) {
    notificationPoll();
  }
});