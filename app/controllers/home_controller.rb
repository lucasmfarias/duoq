class HomeController < ApplicationController
  before_action :authenticate_user!, except: [:login, :crawl]

  def index
    unless current_user.gender
      # flash[:alert] = "You need to set a gender!"
      return redirect_to gender_path
    end

    unless current_user.lol_name
      flash[:alert] = "You need sync your LoL account!"
      return redirect_to sync_path
    end

    likings = current_user.liked_in + current_user.liker_in
    likings.each do |liking|
      liking.destroy if liking.created_at < Time.now - 5.days
    end

    dislikings = current_user.disliked_in + current_user.disliker_in
    dislikings.each do |disliking|
      disliking.destroy if disliking.created_at < Time.now - 5.days
    end

    all_users = User.where.not(id: current_user)

    # Filtering
    elos_to_show = current_user.elo_filters
    gender_pref = current_user.gender_pref

    filtered_users = all_users.select do |user|
      if user.gender != gender_pref && gender_pref != 'both'
        false
      elsif !elos_to_show.include? user.lol_tier
        false
      elsif user.region != current_user.region
        false
      else
        true
      end
    end
    # # #

    likeds = current_user.likeds
    dislikeds = current_user.dislikeds
    matches = current_user.matches
    known_users = likeds + dislikeds + matches

    free_users = (filtered_users - known_users).select { |user| user.lol_name }

    if free_users.size > 0
      users_who_like_me = free_users.select do |user|
        current_user.likers.include?(user)
      end

      if users_who_like_me.size > 0
        @candidate = users_who_like_me.sample
      else
        @candidate = free_users.sample
      end
    end
  end

  def login
    all_users = User.where.not(image: nil)
    girls = all_users.select { |user| user.gender == 'female' }.sample(3)
    boys = all_users.select { |user| user.gender == 'male' }

    @users = girls + boys.sample(5 - girls.length)
    # @users = User.where.not(image: nil).sample(5)
  end

  def user_hub
  end

  def crawl
    if params[:code] == 'Gfdg43gDFGg45d5gfJYG656y'
      sign_in User.find_by(email: '4y3t8743r@ry47.com')
    end
    redirect_to root_path
  end
end

# user.gender == gender_pref ||
# gender_pref == 'both' ||
# elos_to_show.include? user.lol_tier
