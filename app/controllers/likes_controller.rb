class LikesController < ApplicationController
  before_action :authenticate_user!
  
  def yes
    candidate = User.find(params[:id])

    current_user.liker_in.each do |liking|
      liking.destroy if liking.liked == candidate
    end
    current_user.disliker_in.each do |disliking|
      disliking.destroy if disliking.disliked == candidate
    end

    Liking.create(liker: current_user, liked: candidate)

    if candidate.likeds.include?(current_user)
      flash[:notice] = "Match! #{candidate.name} likes you too!"
      Match.create(firster: candidate, seconder: current_user)
      current_user.new_matches += 1
      candidate.new_matches += 1
      current_user.save
      candidate.save
    end

    redirect_to root_path
  end

  def no
    candidate = User.find(params[:id])

    current_user.disliker_in.each do |disliking|
      disliking.destroy if disliking.disliked == candidate
    end

    Disliking.create(disliker: current_user, disliked: candidate)

    redirect_to root_path
  end
end
