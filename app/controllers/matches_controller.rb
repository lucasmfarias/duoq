class MatchesController < ApplicationController
  before_action :authenticate_user!

  def index
    current_user.new_matches = 0
    current_user.save

    @matches = current_user.matches
  end

  def show
    @match = User.find(params[:id])
    unless current_user.matches.include?(@match)
      redirect_to matches_path
    end
  end

  def destroy
    @match = User.find(params[:id])
    if current_user.matches.include?(@match)
      matches = current_user.first_in + current_user.second_in
      matches.each do |match|
        match.destroy if match.firster == @match || match.seconder == @match
      end
    end

    current_user.disliker_in.each do |disliking|
      disliking.destroy if disliking.disliked == @match
    end
    current_user.liker_in.each do |liking|
      liking.destroy if liking.liked == @match
    end

    Disliking.create(disliker: current_user, disliked: @match)

    redirect_to matches_path
  end
end
