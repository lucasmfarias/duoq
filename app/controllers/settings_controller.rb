class SettingsController < ApplicationController
  before_action :authenticate_user!

  def index
  end

  def gender
    redirect_to root_path if current_user.gender
  end

  def set_gender
    current_user.update!(settings_params)

    redirect_to root_path
  end

  def update
    current_user.update!(settings_params)

    redirect_to settings_path
  end

  private

  def settings_params
    allowed_params = [
      :gender_pref,
      :unranked_filter,
      :iron_filter,
      :bronze_filter,
      :silver_filter,
      :gold_filter,
      :platinum_filter,
      :diamond_filter,
      :master_filter,
      :grandmaster_filter,
      :challenger_filter
    ]
    allowed_params << :gender unless current_user.gender

    params.require(:user).permit(allowed_params)
  end
end
