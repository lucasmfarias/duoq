class SyncController < ApplicationController
  before_action :authenticate_user!

  include SyncHelper

  REGIONS =
  [['Brazil', 'BR1'], ['North America', 'NA1'], ['Europe West', 'EUW1'], ['Korea', 'KR'],  ['Oceania', 'OC1'], ['Japan', 'JP1'], ['Europe Nordic & East', 'EUN1'], ['Russia', 'RU'], ['Turkey', 'TR1'], ['Latin America North', 'LA1'], ['Latin America South', 'LA2']]

  def index
    @regions = REGIONS
    unless current_user.lol_verification_key
      generate_lol_verification_key(current_user)
    end
  end

  def verify
    unless current_user.can_sync?
      time_left = 120 - (Time.now - current_user.synced_at)
      flash[:alert] = "#{time_left.seconds.to_i}s before you can sync again"
      return redirect_to sync_path
    end

    lol_name = params[:lol_name]
    region = params[:lol_region]

    valid, message = verify_lol_username(lol_name, region, current_user)

    if valid
      flash[:notice] = message
      current_user.just_synced
    else
      flash[:alert] = message
    end

    redirect_to sync_path
  end

  def refresh
    unless current_user.can_sync?
      time_left = 120 - (Time.now - current_user.synced_at)
      flash[:alert] = "#{time_left.seconds.to_i}s before you can sync again"
      return redirect_to sync_path
    end

    if refresh_sync(current_user)
      flash[:notice] = current_user.name + " updated"
      current_user.just_synced
    else
      flash[:alert] = "Couldn't refresh your LoL sync, are you sure you're synced and ranked?"
    end
    redirect_to sync_path
  end

  def desync
    current_user.lol_name = nil
    current_user.lol_tier = nil
    current_user.lol_rank = nil
    current_user.lol_main = nil
    current_user.lol_region = nil
    current_user.save

    flash[:notice] = "LoL account desynced"
    redirect_to sync_path
  end
end
