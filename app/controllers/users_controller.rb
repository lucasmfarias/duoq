class UsersController < ApplicationController
  before_action :authenticate_user!

  def edit_profile
    @user = current_user
  end

  def update_profile
    @user = current_user

    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to profile_path, notice: 'Profile was successfully updated.' }
        format.js
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { redirect_to profile_path, alert: @user.errors.full_messages[0] }
        format.js
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def user_params
    params.require(:user).permit(:image, :bio)
  end
end
