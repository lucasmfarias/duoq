module SyncHelper
  require 'securerandom'
  require 'open-uri'
  include ERB::Util

  RIOT_API_URL = "https://.api.riotgames.com/lol/"
  SUMM_URL = RIOT_API_URL + "summoner/v4/summoners/by-name/"
  CODE_URL = RIOT_API_URL + "platform/v4/third-party-code/by-summoner/"
  ELO_URL = RIOT_API_URL + "league/v4/positions/by-summoner/"
  MAINS_URL = RIOT_API_URL + "champion-mastery/v4/champion-masteries/by-summoner/"

  ID_CHAMPION = {"266"=>"Aatrox", "103"=>"Ahri", "84"=>"Akali", "12"=>"Alistar", "32"=>"Amumu", "34"=>"Anivia", "1"=>"Annie", "22"=>"Ashe", "136"=>"Aurelion Sol", "268"=>"Azir", "432"=>"Bardo", "53"=>"Blitzcrank", "63"=>"Brand", "201"=>"Braum", "51"=>"Caitlyn", "164"=>"Camille", "69"=>"Cassiopeia", "31"=>"Cho'Gath", "42"=>"Corki", "122"=>"Darius", "131"=>"Diana", "119"=>"Draven", "36"=>"Dr. Mundo", "245"=>"Ekko", "60"=>"Elise", "28"=>"Evelynn", "81"=>"Ezreal", "9"=>"Fiddlesticks", "114"=>"Fiora", "105"=>"Fizz", "3"=>"Galio", "41"=>"Gangplank", "86"=>"Garen", "150"=>"Gnar", "79"=>"Gragas", "104"=>"Graves", "120"=>"Hecarim", "74"=>"Heimerdinger", "420"=>"Illaoi", "39"=>"Irelia", "427"=>"Ivern", "40"=>"Janna", "59"=>"Jarvan IV", "24"=>"Jax", "126"=>"Jayce", "202"=>"Jhin", "222"=>"Jinx", "145"=>"Kai'Sa", "429"=>"Kalista", "43"=>"Karma", "30"=>"Karthus", "38"=>"Kassadin", "55"=>"Katarina", "10"=>"Kayle", "141"=>"Kayn", "85"=>"Kennen", "121"=>"Kha'Zix", "203"=>"Kindred", "240"=>"Kled", "96"=>"Kog'Maw", "7"=>"LeBlanc", "64"=>"Lee Sin", "89"=>"Leona", "127"=>"Lissandra", "236"=>"Lucian", "117"=>"Lulu", "99"=>"Lux", "54"=>"Malphite", "90"=>"Malzahar", "57"=>"Maokai", "11"=>"Master Yi", "21"=>"Miss Fortune", "62"=>"Wukong", "82"=>"Mordekaiser", "25"=>"Morgana", "267"=>"Nami", "75"=>"Nasus", "111"=>"Nautilus", "518"=>"Neeko", "76"=>"Nidalee", "56"=>"Nocturne", "20"=>"Nunu", "2"=>"Olaf", "61"=>"Orianna", "516"=>"Ornn", "80"=>"Pantheon", "78"=>"Poppy", "555"=>"Pyke", "133"=>"Quinn", "497"=>"Rakan", "33"=>"Rammus", "421"=>"Rek'Sai", "58"=>"Renekton", "107"=>"Rengar", "92"=>"Riven", "68"=>"Rumble", "13"=>"Ryze", "113"=>"Sejuani", "35"=>"Shaco", "98"=>"Shen", "102"=>"Shyvana", "27"=>"Singed", "14"=>"Sion", "15"=>"Sivir", "72"=>"Skarner", "37"=>"Sona", "16"=>"Soraka", "50"=>"Swain", "517"=>"Sylas", "134"=>"Syndra", "223"=>"Tahm Kench", "163"=>"Taliyah", "91"=>"Talon", "44"=>"Taric", "17"=>"Teemo", "412"=>"Thresh", "18"=>"Tristana", "48"=>"Trundle", "23"=>"Tryndamere", "4"=>"Twisted Fate", "29"=>"Twitch", "77"=>"Udyr", "6"=>"Urgot", "110"=>"Varus", "67"=>"Vayne", "45"=>"Veigar", "161"=>"Vel'Koz", "254"=>"Vi", "112"=>"Viktor", "8"=>"Vladimir", "106"=>"Volibear", "19"=>"Warwick", "498"=>"Xayah", "101"=>"Xerath", "5"=>"Xin Zhao", "157"=>"Yasuo", "83"=>"Yorick", "154"=>"Zac", "238"=>"Zed", "115"=>"Ziggs", "26"=>"Zilean", "142"=>"Zoe", "143"=>"Zyra"}


  VALID_REGIONS =
  ['RU', 'KR', 'BR1', 'OC1', 'JP1', 'NA1', 'EUN1', 'EUW1', 'TR1', 'LA1', 'LA2']

  def generate_lol_verification_key(user)
    user.lol_verification_key = "#{SecureRandom.hex(4)}".upcase
    user.save
  end

  def verify_lol_username(lol_name, region, user)
    return [false, "Invalid region"] unless VALID_REGIONS.include?(region)

    summoner_id, summoner_name = get_summoner_id_name(lol_name, region)

    return [false, "Could not retrieve summoner name"] unless summoner_name

    unless check_verification_key(summoner_id, user.lol_verification_key, region)
      return [false, "The verification key does not match. Please double check your verification key and LoL username"]
    end

    tier, rank = get_tier_rank(summoner_id, region)

    main_id = get_main(summoner_id, region)
    main = ID_CHAMPION[main_id]

    # clean_region = region.delete('0-9')

    if User.exists?(lol_name: summoner_name, lol_region: region)
      duplicate_user = User.find_by(lol_name: summoner_name, lol_region: region)
      duplicate_user.lol_name = nil
      duplicate_user.lol_tier = nil
      duplicate_user.lol_rank = nil
      duplicate_user.lol_main = nil
      duplicate_user.lol_region = nil
      duplicate_user.save
    end

    user.lol_name = summoner_name
    user.lol_tier = tier
    user.lol_rank = rank
    user.lol_main = main
    user.lol_region = region
    if user.save
      return [true, "LoL account synced!"]
    else
      return [false, "Could not sync your LoL account, please try again later"]
    end
  end

  def refresh_sync(user)
    return false unless user.lol_name

    region = user.lol_region
    summoner_id, summoner_name = get_summoner_id_name(user.lol_name, region)

    tier, rank = get_tier_rank(summoner_id, region)

    if tier
      user.lol_tier = tier
      user.lol_rank = rank
      user.save
      return true
    else
      return false
    end
  end

  private

  def get_summoner_id_name(summoner_name, region)
    return false if summoner_name.length > 16 || summoner_name.length < 1

    region_url = SUMM_URL.dup
    region_url = insert_region_into_url(region_url, region)

    url_rdy_name = url_encode(summoner_name)

    begin
      url = open(region_url + "#{url_rdy_name}?api_key=#{ENV["RIOT_KEY"]}").string
    rescue
      return false
    end

    summoner_hash = JSON.parse url
    [summoner_hash["id"], summoner_hash["name"]]
  end

  def check_verification_key(summoner_id, key, region)
    region_url = CODE_URL.dup
    region_url = insert_region_into_url(region_url, region)

    begin
      url = open(region_url + "#{summoner_id}?api_key=#{ENV["RIOT_KEY"]}").string
    rescue
      # CHEATING VERIFICATION! CHANGE BEFORE LAUNCH!
      return false
      # return true
    end

    verification_hash = JSON.parse url

    # CHEATING VERIFICATION! CHANGE BEFORE LAUNCH!
    return verification_hash.upcase == key
    # return true
  end

  def get_tier_rank(summoner_id, region)
    return false unless summoner_id

    region_url = ELO_URL.dup
    region_url = insert_region_into_url(region_url, region)

    begin
      url = open(region_url + "#{summoner_id}?api_key=#{ENV["RIOT_KEY"]}").string
    rescue
      return false
    end

    rank_array = JSON.parse url

    tier = nil
    rank = nil
    rank_array.each do |queue_hash|
      next unless queue_hash["queueType"] == "RANKED_SOLO_5x5"
      tier = queue_hash["tier"]
      rank = queue_hash["rank"]
    end

    [tier, rank]
  end

  def get_main(summoner_id, region)
    region_url = MAINS_URL.dup
    region_url = insert_region_into_url(region_url, region)

    begin
      url = open(region_url + "#{summoner_id}?api_key=#{ENV["RIOT_KEY"]}").read
    rescue
      return false
    end

    mains_array = JSON.parse url

    mains_array[0]['championId'].to_s if mains_array[0]
  end

  def insert_region_into_url(url, region)
    url.insert(8, region)
  end
end
