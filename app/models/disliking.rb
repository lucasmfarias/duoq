class Disliking < ApplicationRecord
  belongs_to :disliker, :class_name => "User", :foreign_key => "disliker_id"
  belongs_to :disliked, :class_name => "User", :foreign_key => "disliked_id"
end
