class Liking < ApplicationRecord
  belongs_to :liker, :class_name => "User", :foreign_key => "liker_id"
  belongs_to :liked, :class_name => "User", :foreign_key => "liked_id"
end
