class User < ApplicationRecord
  # Likes
  has_many :liker_in, class_name: "Liking", :foreign_key => "liker_id", dependent: :destroy
  has_many :liked_in, class_name: "Liking", :foreign_key => "liked_id", dependent: :destroy
  has_many :likers, through: :liked_in, :foreign_key => "liker_id"
  has_many :likeds, through: :liker_in, :foreign_key => "liked_id"

  # Dislikes
  has_many :disliker_in, class_name: "Disliking", :foreign_key => "disliker_id", dependent: :destroy
  has_many :disliked_in, class_name: "Disliking", :foreign_key => "disliked_id", dependent: :destroy
  has_many :dislikers, through: :disliked_in, :foreign_key => "disliker_id"
  has_many :dislikeds, through: :disliker_in, :foreign_key => "disliked_id"

  # Matches
  has_many :first_in, class_name: "Match", :foreign_key => "first_id", dependent: :destroy
  has_many :second_in, class_name: "Match", :foreign_key => "second_id", dependent: :destroy
  has_many :firsters, through: :second_in
  has_many :seconders, through: :first_in

  # Include default devise modules. Others available are:
  # , :lockable, :timeoutable, :trackable and :omniauthable
  devise :confirmable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :omniauthable, omniauth_providers: [:facebook]

  mount_uploader :image, ImageUploader

  validates :bio, length: { maximum: 140 }
  validates :gender, inclusion: { in: %w(male female) }, :allow_nil => true
  validates :gender_pref, inclusion: { in: %w(male female both) }

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0, 20]

      user.skip_confirmation!
    end
  end

  def name
    lol_name || 'Unsynced user'
  end

  def elo
    lol_tier ? (lol_tier.to_s + " " + lol_rank.to_s) : 'UNRANKED'
  end

  def region
    lol_region ? lol_region.delete('0-9') : ''
  end

  def matches
    matchings.map do |matching|
      if matching.firster == self
        matching.seconder
      else
        matching.firster
      end
    end
  end

  def matchings
    (first_in + second_in).sort! { |x, y| y.created_at <=> x.created_at }
  end

  def elo_filters
    filter = []
    filter << nil if unranked_filter
    filter << 'IRON' if iron_filter
    filter << 'BRONZE' if bronze_filter
    filter << 'SILVER' if silver_filter
    filter << 'GOLD' if gold_filter
    filter << 'PLATINUM' if platinum_filter
    filter << 'DIAMOND' if diamond_filter
    filter << 'MASTER' if master_filter
    filter << 'GRANDMASTER' if grandmaster_filter
    filter << 'CHALLENGER' if challenger_filter
    return filter
  end

  def photo
    if image?
      image.url
    else
      'default.png'
    end
  end

  def badge
    "badges/#{lol_tier.downcase}.png"
  end

  def just_synced
    self.synced_at = Time.now
    self.save
  end

  def can_sync?
    if synced_at
      synced_at < Time.now - 2.minutes
    else
      true
    end
  end

end
