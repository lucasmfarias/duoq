Rails.application.routes.draw do
  get 'notifications', to: 'notifications#index'

  get 'settings', to: 'settings#index'
  patch 'settings', to: 'settings#update'
  get 'gender', to: 'settings#gender'
  patch 'gender', to: 'settings#set_gender'

  get 'sync', to: 'sync#index'
  post 'sync', to: 'sync#verify'
  get 'refresh_sync', to: 'sync#refresh'
  delete 'sync', to: 'sync#desync'

  get 'matches', to: 'matches#index'
  get 'matches/:id', to: 'matches#show'
  post 'unmatch', to: 'matches#destroy'

  get 'my_profile', to: 'users#my_profile'
  get 'profile', to: 'users#edit_profile'
  post 'profile', to: 'users#update_profile'

  post 'like', to: 'likes#yes'
  post 'dislike', to: 'likes#no'

  get 'user_hub', to: 'home#user_hub'

  devise_for :users, controllers:
    { omniauth_callbacks: 'users/omniauth_callbacks' }

  authenticated :user do
    root 'home#index', as: 'authenticated_root'
  end
  devise_scope :user do
    root 'home#login'
  end

  get 'crawl', to: 'home#crawl'
end
