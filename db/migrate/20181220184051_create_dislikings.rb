class CreateDislikings < ActiveRecord::Migration[5.2]
  def change
    create_table :dislikings do |t|
      t.integer :disliker_id
      t.integer :disliked_id

      t.timestamps
    end
  end
end
