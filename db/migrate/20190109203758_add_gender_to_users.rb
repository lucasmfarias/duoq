class AddGenderToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :gender, :string
    add_column :users, :gender_pref, :string, default: 'both'

    add_column :users, :iron_filter, :boolean, default: true
    add_column :users, :bronze_filter, :boolean, default: true
    add_column :users, :silver_filter, :boolean, default: true
    add_column :users, :gold_filter, :boolean, default: true
    add_column :users, :platinum_filter, :boolean, default: true
    add_column :users, :diamond_filter, :boolean, default: true
    add_column :users, :master_filter, :boolean, default: true
    add_column :users, :grandmaster_filter, :boolean, default: true
    add_column :users, :challenger_filter, :boolean, default: true
  end
end
