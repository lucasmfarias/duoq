class AddUnseenMatchesToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :new_matches, :integer, :default => 0
  end
end
