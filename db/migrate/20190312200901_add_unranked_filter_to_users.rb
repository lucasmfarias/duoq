class AddUnrankedFilterToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :unranked_filter, :boolean, default: true
  end
end
