# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_03_12_200901) do

  create_table "dislikings", force: :cascade do |t|
    t.integer "disliker_id"
    t.integer "disliked_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "likings", force: :cascade do |t|
    t.integer "liker_id"
    t.integer "liked_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "matches", force: :cascade do |t|
    t.integer "first_id"
    t.integer "second_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "lol_name"
    t.string "lol_tier"
    t.string "lol_rank"
    t.string "lol_main"
    t.string "lol_region"
    t.string "lol_verification_key"
    t.string "image"
    t.string "bio"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "provider"
    t.string "uid"
    t.string "gender"
    t.string "gender_pref", default: "both"
    t.boolean "iron_filter", default: true
    t.boolean "bronze_filter", default: true
    t.boolean "silver_filter", default: true
    t.boolean "gold_filter", default: true
    t.boolean "platinum_filter", default: true
    t.boolean "diamond_filter", default: true
    t.boolean "master_filter", default: true
    t.boolean "grandmaster_filter", default: true
    t.boolean "challenger_filter", default: true
    t.integer "new_matches", default: 0
    t.datetime "synced_at"
    t.boolean "unranked_filter", default: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
