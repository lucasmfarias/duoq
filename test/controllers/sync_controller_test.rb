require 'test_helper'

class SyncControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get sync_index_url
    assert_response :success
  end

  test "should get verify" do
    get sync_verify_url
    assert_response :success
  end

  test "should get refresh" do
    get sync_refresh_url
    assert_response :success
  end

end
